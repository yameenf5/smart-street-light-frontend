import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import { Navbar } from "./components"
import { Controls, LandingPage, Monitoring, Nodes } from "./pages"
import { Toaster } from "react-hot-toast"

export default function AppWrapper() {
  const queryClient = new QueryClient()
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Toaster />
          <App />
        </BrowserRouter >
      </QueryClientProvider >
    </>
  )
}

function App() {
  return (<>
    <Navbar />
    <Routes>
      <Route path="/" Component={LandingPage} />
      <Route path="/nodes" Component={Nodes} />
      <Route path="/monitor" Component={Monitoring} />
      <Route path="/controls" Component={Controls} />
    </Routes >
  </>)
}

