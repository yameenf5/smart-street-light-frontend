import { useMutation, useQuery } from "@tanstack/react-query"
import { deleteNode, getNodes, pingNode, switchNode } from "./handleNodes"
import { useState } from "react"

export function Nodes() {
  const { data } = useQuery(['getNodes'], getNodes, {
    refetchInterval: 3000,
    refetchIntervalInBackground: true
  })
  if (!data) return <div>loading...</div>
  const clients: Map<string, { overridden: boolean, countLaneA: number, countLaneB: number }> = new Map(data.clients)
  return (<>
    <section className="mx-auto  lg:flex lg:h-screen lg:items-center bg-gray-700 w-full">
      <div className="max-w-screen-xl px-4 py-8 mx-auto sm:px-6 sm:py-12 lg:px-8">
        <header className="text-center">
        </header>
        <div className=""></div>
        <ul className="grid grid-cols-1 gap-4 mt-8 lg:grid-cols-3 lg:mx-auto md:ml-44">
          {data.section.map((sectionId: string) => <Node sectionId={sectionId} clients={clients} key={crypto.randomUUID()} />)}
        </ul>
      </div>
    </section>
  </>)
}


function Node({ sectionId, clients }: { sectionId: string, clients: Map<string, { overridden: boolean, countLaneA: number, countLaneB: number }> }) {
  const client = clients.get(sectionId)
  const { mutate: pingNodeFn, } = useMutation(['pingSection'], pingNode,)
  const { mutate: deleteNodeFn, } = useMutation(['deleteNode'], deleteNode)
  if (!client) return <>client not found</>
  return (<>
    <li>
      <SwitchModal clientId={sectionId} />
      <div className="card w-96 h-96 shadow-lg mt-10 bg-gray-900 rounded-none hover:h-30 transition-transform transform hover:scale-105 duration-500">
        <div className="card-body">
          <h2 className="card-title uppercase font-mono mx-auto underline bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text font-extrabold text-transparent">Client-Id : {sectionId}</h2>
          <div className="text-sm text-white text-center border border-white w-full px-10 pb-10 pt-5">
            <h2 className="uppercase font-bold font-mono text-2xl mb-4">Client Status</h2>
            <p className="uppercase font-mono ">{client.countLaneA === 0 ? "Lane A Lights are off" : "Lane A Lights are on"}</p>
            <p className="uppercase font-mono ">{client.countLaneB === 0 ? "Lane B Lights are off" : "Lane B Lights are on"}</p>
          </div>
          <div className="card-actions justify-center mt-3">
            <button className="btn bg-black text-white font-mono rounded-none"
              onClick={() => pingNodeFn({ clientId: sectionId })}
            >Ping </button>
            <button className="btn bg-black text-white font-mono rounded-none"
              onClick={() => deleteNodeFn({ clientId: sectionId })}
            >Delete</button>
            <label htmlFor="my_modal_7" className="btn bg-black text-white font-mono rounded-none">switch</label>
          </div>
        </div>
      </div>
    </li>
  </>)
}

function SwitchModal({ clientId }: { clientId: string }) {
  const [currentPosition, setCurrentPostion] = useState(0)
  const [nextPosition, setNextPostion] = useState(0)
  const { mutate: switchNodeFn, } = useMutation(['switchNode'], switchNode)
  return (<>
    <input type="checkbox" id="my_modal_7" className="modal-toggle" />
    <div className="modal">
      <div className="modal-box text-center w-fit">
        <div className="form-control w-full ">
          <label className="label">
            <span className="label-text uppercase font-mono">current position</span>
          </label>
          <input type="text" placeholder="Type here" className="input input-bordered w-full max-w-xs"
            onChange={(e) => setCurrentPostion(parseInt(e.target.value))}
          />
        </div>
        <div className="form-control w-full ">
          <label className="label">
            <span className="label-text uppercase font-mono"
            >next position</span>
          </label>
          <input type="text" placeholder="Type here" className="input input-bordered w-full max-w-xs"
            onChange={(e) => setNextPostion(parseInt(e.target.value))}
          />
        </div>
        <button className="btn bg-black text-white font-mono rounded-none mt-3"
          onClick={() => switchNodeFn({ clientId: clientId, currentPosition: currentPosition, newPosition: nextPosition })}
        >change</button>
      </div>
      <label className="modal-backdrop" htmlFor="my_modal_7">Close</label>
    </div>
  </>)
}
