import toast from "react-hot-toast";

export async function getNodes() {
  const response = await fetch("http://localhost:3000/nodes", {
    method: "GET",
  })
  return await response.json();
}

export async function pingNode({ clientId }: { clientId: string }) {
  try {
    const response = await fetch(`http://localhost:3000/nodes/ping`, {
      method: "POST",
      body: JSON.stringify({ clientId }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    toast.success("NODE PINGED");
    return await response.json();
  } catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function deleteNode({ clientId }: { clientId: string }) {
  try {
    const response = await fetch(`http://localhost:3000/nodes/delete`, {
      method: "POST",
      body: JSON.stringify({ clientId }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    toast.success("NODE DELETED");
    return await response.json();
  }
  catch (error) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function switchNode({ clientId, currentPosition, newPosition }: { clientId: string, currentPosition: number, newPosition: number }) {
  try {
    const response = await fetch(`http://localhost:3000/nodes/switch`, {
      method: "POST",
      body: JSON.stringify({ clientId, currentPosition, newPosition }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    toast.success("NODE SWITCHED");
    return await response.json();
  }
  catch (error) {
    toast.error("INTERNAL SERVER ERROR");
  }
}
