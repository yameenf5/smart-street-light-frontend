import { useQuery } from "@tanstack/react-query"
import { getNodes } from "../Nodes/handleNodes"

export function Monitoring() {
  const { data } = useQuery(['getNodes'], getNodes, {
    refetchInterval: 3000,
    refetchIntervalInBackground: true
  })
  if (!data) return <div>loading...</div>
  const clients: Map<string, { overridden: boolean, countLaneA: number, countLaneB: number }> = new Map(data.clients)
  return (<>
    <section className="mx-auto lg:flex lg:h-screen lg:items-center bg-gray-700 w-full">
      <div className="max-w-screen-xl px-4 py-8 mx-auto sm:px-6 sm:py-12 lg:px-8">
        <header className="text-center">
        </header>
        <div className=""></div>
        <ul className="grid grid-cols-1 gap-4 mt-8 lg:grid-cols-3 lg:mx-auto md:ml-44">
          {data.section.map((sectionId: string) => <Node sectionId={sectionId} clients={clients} key={crypto.randomUUID()} />)}
        </ul>
      </div>
    </section>
  </>)
}


function Node({ sectionId, clients }: { sectionId: string, clients: Map<string, { overridden: boolean, countLaneA: number, countLaneB: number }> }) {
  const client = clients.get(sectionId)
  if (!client) return <>client not found</>
  return (<>
    <li>
      <div className="card w-96 h-96 shadow-lg mt-10 bg-gray-900 rounded-none hover:h-30 transition-transform transform hover:scale-105 duration-500">
        <div className="card-body">
          <h2 className="card-title uppercase font-mono mx-auto underline bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text font-extrabold text-transparent">Client-Id : {sectionId}</h2>
          <div className="text-sm text-white text-center border border-white w-full px-10 pb-10 pt-5">
            <h2 className="uppercase font-mono text-xl mb-4">Client Traffic Monitoring</h2>
            <p className="uppercase font-mono ">Cars in Lane A : {client.countLaneA}</p>
            <p className="uppercase font-mono ">Cars in lane B : {client.countLaneB}</p>
            <p className="uppercase font-mono mt-5">State : {client.overridden ? "Monitoring only" : "monitoring and lighting" }</p>
          </div>
        </div>
      </div>
    </li>
  </>)
}
