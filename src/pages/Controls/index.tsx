import { useMutation, useQuery } from "@tanstack/react-query"
import { getNodes } from "../Nodes/handleNodes"
import { laneHigh, laneLow, roadHigh, roadLow, schedule, sectionHigh, sectionLow, toggleMonitorMode, toggleTime } from "./handleControls"
import { useState } from "react"

export function Controls() {
  const [client, setClient] = useState("none");
  const [lane, setLane] = useState("none");
  const [tLane, setTLane] = useState("none");
  const [time, setTime] = useState<"" | number>("");
  const { data } = useQuery(['getNodes'], getNodes, {
    refetchInterval: 3000,
    refetchIntervalInBackground: true
  })
  const { mutate: toggleTimeFn } = useMutation(['toggleTime'], toggleTime)
  const { mutate: toggleMonitorModeFn } = useMutation(['toggleMode'], toggleMonitorMode)
  const { mutate: sectionHighFn } = useMutation(['sectionHigh'], sectionHigh)
  const { mutate: sectionLowFn } = useMutation(['sectionLow'], sectionLow)
  const { mutate: laneHighFn } = useMutation(['laneHigh'], laneHigh)
  const { mutate: laneLowFn } = useMutation(['laneLow'], laneLow)
  const { mutate: roadHighFn } = useMutation(['roadHigh'], roadHigh)
  const { mutate: roadLowFn } = useMutation(['roadLow'], roadLow)
  const { mutate: scheduleFn } = useMutation(['schedule'], schedule)
  if (!data) return <div>loading...</div>
  const clients: Map<string, { overridden: boolean, countLaneA: number, countLaneB: number, isDayTime: boolean }> = new Map(data.clients)
  const zeroClient = clients.get(data.section[0])
  return (<>
    <section className="mx-auto lg:h-screen lg:flex lg:items-center bg-gray-700 w-full">
      <div className="mx-auto  grid lg:grid-cols-3 grid-cols-1 md:grid-cols-2 lg:mt-16 m-0 ">

        <div className="card w-96 bg-base-100 shadow-xl m-2">
          <div className="card-body">
            <h2 className="card-title font-mono uppercase mx-auto">toggle-Control-Mode</h2>
            <p className="font-mono mx-auto">Mode : {(zeroClient?.overridden && zeroClient.isDayTime) ? "Control Mode and Day" : (zeroClient?.overridden && !zeroClient.isDayTime) ? "Control Mode" : "General Mode"}</p>
            <button className="btn mt-1"
              onClick={() => toggleMonitorModeFn()}
            >TOGGLE</button>
          </div>
        </div>

        <div className="card w-96 bg-base-100 shadow-xl m-2">
          <div className="card-body">
            <h2 className="card-title font-mono uppercase mx-auto">Toggle-Time</h2>
            <p className="mx-auto font-mono">Current-Time : {zeroClient?.isDayTime ? "DAY" : "NIGHT"}</p>
            <button className="btn mt-1"
              onClick={() => toggleTimeFn()}
            >TOGGLE</button>
          </div>
        </div>

        <div className="card w-96 bg-base-100 shadow-xl m-2">
          <div className="card-body">
            <h2 className="card-title font-mono uppercase mx-auto">toggle-section</h2>
            <div className="form-control mx-auto">
              <div className="input-group">
                <select className="select select-bordered"
                  value={client}
                  onChange={(e) => setClient(e.target.value)}
                >
                  <option >Pick Client</option>
                  {data.section.map((sectionId: string) => <option key={sectionId} value={sectionId}>{sectionId}</option>)}
                </select>
                <select className="select select-bordered  ml-1"
                  value={lane}
                  onChange={(e) => setLane(e.target.value)}
                >
                  <option >Pick Lane</option>
                  <option value={"laneA"}>Lane A</option>
                  <option value={"laneB"}>Lane B</option>
                </select>
              </div>
              <button className="btn mt-1"
                onClick={() => sectionHighFn({ clientId: client, laneName: lane })}
              >On</button>
              <button className="btn mt-1"
                onClick={() => sectionLowFn({ clientId: client, laneName: lane })}
              >Off</button>
            </div>
          </div>
        </div>

        <div className="card w-96 bg-base-100 shadow-xl m-2">
          <div className="card-body">
            <h2 className="card-title font-mono uppercase mx-auto">toggle-Lane</h2>
            <select className="select select-bordered  ml-1"
              value={tLane}
              onChange={(e) => setTLane(e.target.value)}
            >
              <option >Pick Lane</option>
              <option value={"laneA"}>Lane A</option>
              <option value={"laneB"}>Lane B</option>
            </select>
            <button className="btn mt-1"
              onClick={() => laneHighFn({ laneName: tLane })}
            >ON</button>
            <button className="btn mt-1"
              onClick={() => laneLowFn({ laneName: tLane })}
            >Off</button>
          </div>
        </div>

        <div className="card w-96 bg-base-100 shadow-xl m-2">
          <div className="card-body">
            <h2 className="card-title font-mono uppercase mx-auto">toggle-Road</h2>
            <p></p>
            <button className="btn mt-1"
              onClick={() => roadHighFn()}
            >ON</button>
            <button className="btn mt-1"
              onClick={() => roadLowFn()}
            >Off</button>
          </div>
        </div>

        <div className="card w-96 bg-base-100 shadow-xl m-2">
          <div className="card-body">
            <h2 className="card-title font-mono uppercase mx-auto">schedule-ligts and control-mode </h2>
            <p>After (sec)</p>
            <input type="number" placeholder="" className="input input-bordered input-md w-full max-w-xs" value={time}
              onChange={(e) => setTime(parseInt(e.target.value))}
            />
          </div>
          <button className="btn m-3"
            onClick={() => scheduleFn({ timeInMin: Number(time) })}
          >Schedule</button>
        </div>
      </div>
    </section>
  </>)
}

