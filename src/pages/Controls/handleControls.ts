import toast from "react-hot-toast";

export async function toggleTime() {
  try {
    const response = await fetch(`http://localhost:3000/controls/toggletime`, {
      method: "POST",
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Time toggled");
    return;
  } catch (err) {
    toast.error("ERROR STATE");
  }
}

export async function sectionHigh({ clientId, laneName }: { clientId: string, laneName: string }) {
  try {
    const response = await fetch('http://localhost:3000/controls/sectionhigh', {
      method: "POST",
      body: JSON.stringify({ clientId, laneName }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Section toggled");
    return;
  }
  catch (err) {
    toast.error("ERROR STATE");
  }
}

export async function sectionLow({ clientId, laneName }: { clientId: string, laneName: string }) {
  try {
    const response = await fetch('http://localhost:3000/controls/sectionlow', {
      method: "POST",
      body: JSON.stringify({ clientId, laneName }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Section toggled");
    return;
  }
  catch (err) {
    toast.error("ERROR STATE");
  }
}

export async function laneHigh({ laneName }: { laneName: string }) {
  try {
    const response = await fetch('http://localhost:3000/controls/lanehigh', {
      method: "POST",
      body: JSON.stringify({ laneName }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Lane toggled");
    return;
  }
  catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function laneLow({ laneName }: { laneName: string }) {
  try {
    const response = await fetch('http://localhost:3000/controls/lanelow', {
      method: "POST",
      body: JSON.stringify({ laneName }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Lane toggled");
    return;
  }
  catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function roadHigh() {
  try {
    const response = await fetch('http://localhost:3000/controls/roadhigh', {
      method: "POST",
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Road toggled");
    return;
  }
  catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function roadLow() {
  try {
    const response = await fetch('http://localhost:3000/controls/roadlow', {
      method: "POST",
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Road toggled");
    return;
  }
  catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function toggleMonitorMode() {
  try {
    const response = await fetch('http://localhost:3000/controls/toggleMonitorMode', {
      method: "POST",
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
    toast.success("Monitor mode toggled");
    return;
  }
  catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function monitorNode({ clientId }: { clientId: string }) {
  try {
    const response = await fetch('http://localhost:3000/controls/monitorNode', {
      method: "POST",
      body: JSON.stringify({ clientId }),
      headers: {
        "Content-Type": "application/json",
      }
    })
    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    toast.success("Node monitored");
    return await response.json();
  }
  catch (err) {
    toast.error("INTERNAL SERVER ERROR");
  }
}

export async function schedule({ timeInMin }: { timeInMin: number }){
  try{
    const response = await fetch('http://localhost:3000/controls/schedule', {
      method: "POST",
      body: JSON.stringify({ timeInMin }),
      headers: {
        "Content-Type": "application/json",
        }
    })
    if (!response.ok) {
      const json = await response.json();
      toast(json.message);
      throw new Error("Network response was not ok");
    }
  }
  catch (err) {
    toast.error("ERROR STATE");
  }
}
