export function LandingPage() {
  return (<>
    <section className="bg-gray-900 text-white">
      <div
        className="mx-auto max-w-screen-xl px-4 py-32 lg:flex lg:h-screen lg:items-center h-screen"
      >
        <div className="mx-auto max-w-3xl text-center">
          <h1
            className="bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-3xl font-extrabold text-transparent sm:text-5xl"
          >
            Smart-Street-Lighting.

            <span className="sm:block"> What it is ? </span>
          </h1>

          <p className="mx-auto mt-4 max-w-xl sm:text-xl/relaxed">
            A project that aim to save the energy by turning on the street lights only when needed.
            A project that aim to automate the process of turning on and off the street lights.
            A project that monitors traffic on the streets.
          </p>

          <div className="mt-8 flex flex-wrap justify-center gap-4">
          </div>
        </div>
      </div>
    </section>
  </>)
}
