import { Link } from "react-router-dom";

export function Navbar() {
  return <>
    <header className="bg-gray-900 border border-gray-500 rounded fixed w-full z-50" >
      <div className="mx-auto max-w-screen-xl px-4 sm:px-6 lg:px-8">
        <div className="flex h-16 items-center justify-between">
          <div className="md:flex md:items-center md:gap-12">
            <Link className="block text-teal-600 dark:text-teal-600" to="/">
              <span className="sr-only">Home</span>
            </Link>
          </div>
          <div className="hidden md:block">
            <nav aria-label="Global">
              <ul className="flex items-center gap-6 text-sm" >
                <li>
                  <Link
                    className="font-normal hover:text-3xl transition-all duration-500 cursor-pointer bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-transparent text-2xl"
                    to="/monitor"
                  >
                    Monitor
                  </Link>
                </li>
                <li>
                  <Link
                    className="font-normal hover:text-3xl transition-all duration-500 cursor-pointer bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-transparent text-2xl"
                    to="/nodes"
                  >
                    Nodes
                  </Link>
                </li>
                <li>
                  <Link
                    className="font-normal hover:text-3xl transition-all duration-500 cursor-pointer bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-transparent text-2xl"
                    to="/controls"
                  >
                    Controls
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
          <div className="flex items-center gap-4">
            <div className="sm:flex sm:gap-4">
            </div>
            {/* drawer */}
            <div className="block md:hidden">
              <div className="drawer drawer-start">
                <input id="my-drawer-4" type="checkbox" className="drawer-toggle" />
                <div className="drawer-content">
                  <label htmlFor="my-drawer-4"
                    className="drawer-button btn rounded bg-gray-100 p-2 text-gray-600 transition hover:text-gray-600/75 dark:bg-gray-800 dark:text-white dark:hover:text-white/75">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4 6h16M4 12h16M4 18h16"
                      />
                    </svg>
                  </label>
                </div>
                <div className="drawer-side">
                  <label htmlFor="my-drawer-4" className="drawer-overlay"></label>
                  <ul className="menu p-4 w-80 h-full text-base-content bg-black">
                    <li>
                      <Link
                        className="font-normal hover:text-3xl transition-all duration-500 cursor-pointer bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-transparent text-2xl"
                        to="/monitor"
                      >
                        Monitor
                      </Link>
                    </li>
                    <li>
                      <Link
                        className="font-normal hover:text-3xl transition-all duration-500 cursor-pointer bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-transparent text-2xl"
                        to="/nodes"
                      >
                        Nodes
                      </Link>
                    </li>
                    <li>
                      <Link
                        className="font-normal hover:text-3xl transition-all duration-500 cursor-pointer bg-gradient-to-r from-green-300 via-blue-500 to-purple-600 bg-clip-text text-transparent text-2xl"
                        to="/controls"
                      >
                        Controls
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  </>
}



